$(document).ready(function(){
		var wHeight = $(window).height();
		$('.header_container').height(wHeight);
	$(window).resize(function(){
		var wHeight = $(window).height();
		$('.header_container').height(wHeight);
	});
	$('.header_container .user_enter .popup_show').click(function(){
		$('.dropdown-menu').slideToggle();
		$(this).toggleClass('open')
	})
	$('.search input').focus(function(){
		$('.search_icon').hide();
		$(this).css('paddingLeft' , '15px')
		$(this).data('placeholder',$(this).attr('placeholder'))
      	$(this).attr('placeholder','');
	});
    $('input,textarea').blur(function(){
      $(this).attr('placeholder',$(this).data('placeholder'));
    });
	$('.search input').focusout(function(){
		var nameLngth = $('.search input').val();
		if (nameLngth == true) {
			$('.search_icon').hide();
			$('.search input').css('paddingLeft' , '15px')
		}
		if (nameLngth == 0) {
			$('.search_icon').show();
			$('.search input').css('paddingLeft' , '40px')
		}
		 if (nameLngth == null) {
		 	$('.search_icon').show();
		 }
	});
	if ($('.ui-front').is(":visible")) {
		$('.search input').css('borderRadius' , '4px 4px 0 0')
	}
	else {
		 $('.search input').css('borderRadius' , '4px')
	}


	$(window).scroll(function(){
        var bo = $("body").scrollTop();
    	if ( bo > 600 ) {
    		$(".world_map_image").animate({'opacity':'1'}, 1000);
    		setTimeout(function(){
				$(".first_plag").animate({'margin-top':'45%'}, 1000);
			}, 1000);
			setTimeout(function(){
				$(".second_plag").animate({'margin-top':'30%'}, 1000);
			}, 2000);
			setTimeout(function(){
				$(".third_plag").animate({'margin-top':'30%'}, 1000);
			}, 3000);
			setTimeout(function(){
				$(".fourth_plag").animate({'margin-top':'55%'}, 1000);
			}, 4000);
    	}
    })




});
